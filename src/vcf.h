
/*
 * Osmo - a handy personal organizer
 *
 * Copyright (C) 2015 Tomasz Mąka <pasp@users.sourceforge.net>
 *               2015 Piotr Mąka <silloz@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef VCF_H
#define	VCF_H

#include <stddef.h>
#include <glib.h>

#define VCF_TYPE_TEL_VOICE "voice"
#define VCF_TYPE_TEL_FAX "fax"
#define VCF_TYPE_TEL_CELL "cell"
#define VCF_TYPE_HOME "home"
#define VCF_TYPE_WORK "work"


typedef struct vcf_writer vcf_writer;

typedef void(vcf_writer_callback) (gchar const *buffer, gsize buffer_size, void *user_data);

vcf_writer *vcf_create_writer(vcf_writer_callback *callback, void *user_data);

void vcf_close_writer(vcf_writer *writer);

void vcf_write_begin(vcf_writer *writer);
void vcf_write_end(vcf_writer *writer);

void vcf_write_property(vcf_writer *writer, gchar const *property_name);
void vcf_write_FN(vcf_writer *writer, gchar const *first_name, gchar const *last_name);
void vcf_write_N(vcf_writer *writer, gchar const *last_name, gchar const *first_name, gchar const *second_name);
void vcf_write_NICKNAME(vcf_writer *writer, gchar const *nick_name);
void vcf_write_BDAY(vcf_writer *writer, guint32 julian_day);
void vcf_write_ADR(vcf_writer *writer, gchar const *type, gchar const *address, gchar const *city, gchar const *state, gchar const *post_code, gchar const *country);
void vcf_write_ORG(vcf_writer *writer, gchar const *organization, gchar const *department);
#define vcf_write_TEL(writer, number, pref, ...) { \
gchar const **types=(gchar const *[]){__VA_ARGS__, NULL}; \
vcf_write_TEL_internal(writer, number, pref, types); \
}
void vcf_write_EMAIL(vcf_writer *writer, gchar const *email, gint pref);
void vcf_write_URL(vcf_writer *writer, gchar const *url);
void vcf_write_URL_pref(vcf_writer *writer, gchar const *url, gint pref);
void vcf_write_IMPP(vcf_writer *writer, gchar const *url, gchar const *type);
void vcf_write_NOTE(vcf_writer *writer, gchar const *note);

/* Do not call the functions below directly, use the provided macros instead. */
void vcf_write_TEL_internal(vcf_writer *writer, gchar const *number, gint pref, gchar const **types);

#endif	/* VCF_H */

